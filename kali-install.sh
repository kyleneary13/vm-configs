#!/bin/bash

if [[ -n "$1" ]]; then
	echo "[*] Running with verbosity!"
else
	exec 3>&1 1>/dev/null 2>&1
fi

# CHECK IF ROOT
if [[ $EUID -ne 0 ]]; then
	echo "[X] This script must be run as root." >&3
	exit 1
fi
echo "[*] User account is root!" >&3

# INITIALIZE
echo "[*] Initializing..." >&3
INIT=$(pwd)
USERACCOUNT="root"
InstallPath=$(pwd)
echo "[*] Initialization complete!" >&3

# UPDATE KALI
echo "[*] Updating..." >&3
apt -y update
apt -y upgrade
apt -y dist-upgrade
apt -y autoremove
apt -y clean
echo "[*] Update compelete!" >&3

# SET BASH PROMPT
echo "[*] Setting custom bash prompt..." >&3
if grep -q "export PS1" /$USERACCOUNT/.bashrc; then
	echo "[*] Custom prompt already set! Moving on." >&3
else
	echo 'export PS1="\[\033[38;5;4m\][\u\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;4m\]@\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;4m\]\h]\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;2m\][\d\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;2m\]\@]\[$(tput sgr0)\]\[\033[38;5;15m\]\n\[$(tput sgr0)\]\[\033[38;5;3m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;3m\]>\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"' >> /$USERACCOUNT/.bashrc
	echo "[*] Custom prompt set!" >&3
fi

# INSTALL APT TOOLS
echo "[*] Installing tools with APT repos..." >&3
apt -y install sl tree synaptic net-tools git curl ncat ifupdown
apt -y install netcat-traditional golang powershell mingw-w64
echo "[*] Installations complete!" >&3

# INSTALL TOOLS IN /OPT
echo "[*] Installing tools with GIT repos..." >&3
cd /opt
mkdir enum
mkdir osint
mkdir exploit
mkdir c2
mkdir obfuscate
mkdir payload
mkdir mitm
mkdir web
mkdir privesc
mkdir phish
mkdir misc
mkdir password
mkdir persist
mkdir password
mkdir cloud
mkdir wifi
mkdir db
mkdir custom
cat $InstallPath/GlobalFiles/Git.txt | while read REPO
do
	cd $(echo $REPO | awk -F, '{print $2}')
	git clone $(echo $REPO | awk -F, '{print $1}')
	cd ..
done
cd /opt/enum/PowerSploit
git pull origin dev
cd /opt
ln -s /usr/share/webshells /opt/web/webshells
ln -s /usr/share/wordlists /opt/password/wordlists
mkdir /opt/misc/SysinternalsSuite
cd /opt/misc/SysinternalsSuite
wget https://download.sysinternals.com/files/SysinternalsSuite.zip
unzip SysinternalsSuite.zip
rm SysinternalsSuite.zip
cd /opt/misc
mkdir JSoup
mkdir Jython
cd JSoup
wget https://jsoup.org/packages/jsoup-1.13.1.jar
cd ..
cd Jython
wget http://search.maven.org/remotecontent?filepath=org/python/jython-standalone/2.7.1/jython-standalone-2.7.1.jar
echo "[*] Installations complete!" >&3

# MOVE FILES TO DOCS FOLDER
echo "[*] Moving custom documents..."
cp -r $InstallPath/GlobalFiles/Clickjack.html /opt/web/
cp -r $InstallPath/GlobalFiles/Ducky.txt /opt/misc/
mkdir /$USERACCOUNT/.ssh
cp -r $InstallPath/GlobalFiles/SSH/* /$USERACCOUNT/.ssh/
mkdir /$USERACCOUNT/Documents/Burp
cp -r $InstallPath/GlobalFiles/Burp /$USERACCOUNT/Documents/Burp
echo "[*] Moving complete!"

# CREATE ALIASES
echo "[*] Creating aliases..." >&3
if grep -q "alias update" /$USERACCOUNT/.bashrc; then
	echo "[*] Bash aliases already created!" >&3
else
	echo 'alias update="apt -y update && apt -y upgrade && apt -y dist-upgrade && apt -y autoremove && apt -y clean"' >> /$USERACCOUNT/.bashrc
	echo 'alias renet="ifdown eth0 && ifup eth0 && service networking restart"' >> /$USERACCOUNT/.bashrc
	mkdir /$USERACCOUNT/.scripts
	cp -r $InstallPath/KaliScripts/* /$USERACCOUNT/.scripts/
	echo 'alias gpull="/root/.scripts/git-pull.sh"' >> /$USERACCOUNT/.bashrc
	echo 'alias zoom="/root/.scripts/display-zoom.sh"' >> /$USERACCOUNT/.bashrc
	echo 'alias shrink="/root/.scripts/display-shrink.sh"' >> /$USERACCOUNT/.bashrc
	echo 'alias vmtools="/etc/init.d/open-vm-tools start && /etc/init.d/open-vm-tools stop && /etc/init.d/open-vm-tools restart"' >> /$USERACCOUNT/.bashrc
	echo "[*] Bash aliases created!" >&3
fi
echo "[*] Done!" >&3
cd $INIT
