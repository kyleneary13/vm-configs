#!/bin/bash

if [[ -n "$1" ]]; then
	echo "[*] Running with verbosity!"
else
	exec 3>&1 1>/dev/null 2>&1
fi

# INITIALIZE
echo "[*] Initializing..." >&3
INIT=$(pwd)
USERACCOUNT=$(whoami)
InstallPath=$(pwd)
echo "[*] Initialization complete!" >&3

# UPDATE UBUNTU
echo "[*] Updating..." >&3
apt -y update
apt -y upgrade
apt -y dist-upgrade
apt -y autoremove
apt -y clean
echo "[*] Update compelete!" >&3

# SET BASH PROMPT
echo "[*] Setting custom bash prompt..." >&3
if grep -q "export PS1" /$USERACCOUNT/.bashrc; then
	echo "[*] Custom prompt already set! Moving on." >&3
else
	echo 'export PS1="\[\033[38;5;4m\][\u\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;4m\]@\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;4m\]\h]\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;2m\][\d\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;2m\]\@]\[$(tput sgr0)\]\[\033[38;5;15m\]\n\[$(tput sgr0)\]\[\033[38;5;3m\]\w\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;3m\]>\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"' >> /$USERACCOUNT/.bashrc
	echo "[*] Custom prompt set!" >&3
fi

# INSTALL APT TOOLS
echo "[*] Installing tools..." >&3
apt -y install sl tree synaptic net-tools git curl ncat ifupdown
apt -y install netcat-traditional golang mingw-w64

# INSTALL OTHER TOOLS
mkdir /opt/SysinternalsSuite
cd /opt/SysinternalsSuite
wget https://download.sysinternals.com/files/SysinternalsSuite.zip
unzip SysinternalsSuite.zip
rm SysinternalsSuite.zip
echo "[*] Installations complete!" >&3

# MOVE FILES TO DOCS FOLDER
echo "[*] Moving custom documents..."
mkdir /$USERACCOUNT/.ssh
cp -r $InstallPath/GlobalFiles/SSH/* /$USERACCOUNT/.ssh/
echo "[*] Moving complete!"

# CREATE ALIASES
echo "[*] Creating aliases..." >&3
if grep -q "alias update" /$USERACCOUNT/.bashrc; then
	echo "[*] Bash aliases already created!" >&3
else
	echo 'alias update="apt -y update && apt -y upgrade && apt -y dist-upgrade && apt -y autoremove && apt -y clean"' >> /$USERACCOUNT/.bashrc
	echo 'alias renet="ifdown eth0 && ifup eth0 && service networking restart"' >> /$USERACCOUNT/.bashrc
	mkdir /$USERACCOUNT/.scripts
	cp -r $InstallPath/KaliScripts/* /$USERACCOUNT/.scripts/
	echo 'alias gpull="/root/.scripts/git-pull.sh"' >> /$USERACCOUNT/.bashrc
	echo 'alias zoom="/root/.scripts/display-zoom.sh"' >> /$USERACCOUNT/.bashrc
	echo 'alias shrink="/root/.scripts/display-shrink.sh"' >> /$USERACCOUNT/.bashrc
	echo 'alias vmtools="/etc/init.d/open-vm-tools start && /etc/init.d/open-vm-tools stop && /etc/init.d/open-vm-tools restart"' >> /$USERACCOUNT/.bashrc
	echo "[*] Bash aliases created!" >&3
fi
echo "[*] Done!" >&3
cd $INIT
