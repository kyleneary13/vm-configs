#!/bin/bash

# INITIALIZE
INIT=$(pwd)
USERACCOUNT="root"
cd /opt
wget https://bitbucket.org/kyleneary13/vm-configs/raw/30d54ebfd1c9976a0b0a416a26986bdf2cc9c2fb/GlobalFiles/Git.txt

# GIT REPOS
cat Git.txt | while read REPO
do
    DIR=$(echo $REPO | awk -F'/' '{print $5}' | awk -F',' '{print $2}')
    FOO=$(echo $REPO | awk -F'/' '{print $5}' | awk -F',' '{print $1}')
    if [ -d "/opt/$DIR/$FOO" ]; then
		cd $DIR
		cd $FOO
		echo $FOO
		git pull
		cd ..
		cd ..
	else
		cd $(echo $REPO | awk -F, '{print $2}')
		git clone $(echo $REPO | awk -F, '{print $1}')
		cd ..
	fi
done

# CORRECT POWERVIEW
cd /opt/enum/PowerSploit
git pull origin dev

# CLEANUP
rm /opt/Git.txt
cd $INIT
