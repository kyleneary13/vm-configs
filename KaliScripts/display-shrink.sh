#!/bin/bash

xfconf-query -c xfwm4 -p /general/theme -s Kali-Dark
xfconf-query -c xsettings -p /Gdk/WindowScalingFactor -n -t 'int' -s 1
cat <<- EOF >> ~/.xsessionrc
        export QT_SCALE_FACTOR=1
        export XCURSOR_SIZE=24
        export GDK_SCALE=1
EOF